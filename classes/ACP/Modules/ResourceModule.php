<?php
/** 
 * Resource module for CodeIgniter
 * @author      leman 
 * @copyright   Copyright (c) 2015, LMSWork. 
 * @link        http://lmswork.com 
 * @since       Version 1.0 
 *  
 */

namespace ACP\Modules{

class ResourceModule extends \ACP\Core\HC_Module
{

    function __construct(){
        parent::__construct();

        // load cache helper if it does not exist
        $CI = &get_instance();
        if(!function_exists('cache_get')){
            $CI->load->helper('cache');
        }
        // load File model if it does not exist
        if(!function_exists('File_model')){
            $CI->load->model('file_model');
        }

        // load File model if it does not exist
        if(!function_exists('Album_model')){
            $CI->load->model(array('album_model','album_photo_model'));
        }
    }

    public function file_path($file_row){

        $url = upload_file($file_row,$this->is_refresh,'files');

        return $url;
    }

    public function file_url($file_row, $options = NULL){

        $url = upload_url($file_row,$this->is_refresh,'files','files',$options);

        return $url;
    }

    public function picture_mapping($file_row,$group='file',$size='thumb',$subpath = false,$options = false,&$image_info=false){
        if(is_array($file_row) && !isset($file_row['id'])){
            $outputs = array();
            foreach($file_row as $file){
                $_image_info = false;
                $image_output = $this->picture_mapping( $file, $group,$size,$subpath,$options,$_image_info);
                if(!empty($image_output)){
                    $outputs[] = $image_output;
                    $image_info[] = $_image_info;
                }
            }
            return $outputs;
        }
        $dest_path = '';
        $dest_path_seg = array();
        
        if(!empty($subpath)){
            if(is_array($subpath)){
                $dest_path = implode('/', $subpath);
                $dest_path_seg = $subpath;
            }else{
                $dest_path = $subpath;
                $dest_path_seg = explode('/', $dest_path);
            }
        }

        if(!$image_info) 
            $image_info = array();

        $croparea = NULL;
        if(isset($options['croparea']))
        	$croparea = $options['croparea'];

        $url = picture_url($file_row,$group,$size, $this->config->item('is_refresh'), $image_info, 'files',$dest_path_seg, $croparea);
        if(empty($url)){
            log_message('error','ResourceModule/picture_mapping, build image for '.$file_row['id'].' at subpath '.$dest_path.'. imagegroup='.$group.', size='.$size);
            return NULL;
        }
        
        if(! preg_match('#^https?\:\/\/#', $url ) ) $url  = base_url($url );

        $output = array();
        $output['url'] = $url;
        $output['width'] = $image_info['width'];
        $output['height'] = $image_info['height'];
        //$output['_raw'] = $image_info;
        return $output;
    }

    public function get_file($file_id, $options = NULL, $cache_ttl = 3600){
        if(!is_string($file_id)){
            log_message('error','ResourceModule::get_file, file id must be an string value.');
            return NULL;
        }
        $cache_key = 'res/file/'.$file_id.'';

        $r = cache_get($cache_key);


        $cache_allowed = false;
        if( is_bool($cache_ttl)){
            $cache_allowed = $cache_ttl;
            $cache_ttl = 3600;
        } else{
            $cache_allowed = true;
        }

        if(!$cache_allowed || empty($r)){

            $r = $this->file_model->read(array('id'=>$file_id));
            if(empty($r['id'])){
                $this->load->helper('error');
                log_message('error', 'ResourceModule/get_file, cannot load data for file '.print_r(compact('file_id'),true).print_plain_backtrace(true));
                return NULL;
            }

            if($cache_ttl>0){
                log_message('debug', 'ResourceModule/get_file, saving cache for file '.$file_id.' by key '.$cache_key);
                cache_set($cache_key, $r,$cache_ttl);
            }
        }else{
            log_message('debug', 'ResourceModule/get_file, getting cache for file '.$file_id.' by key '.$cache_key);
        }
        return $r;
    }

    public function reset_file_cache($r){
        $cache_key = 'res/file/'.$r['id'].'';

        // load cache helper if it does not exist
        $CI = &get_instance();
        if(!function_exists('cache_get')){
            $CI->load->helper('cache');
        }

        log_message('debug', 'ResourceModule/reset_file_cache, removing cache for file '.$r['id'].' by key '.$cache_key);
        cache_remove($cache_key);
    }


    public function get_album($album_id, $options = NULL, $cache_ttl = 3600){

        if(is_array($album_id) && isset($album_id['id'])){
            $options = $album_id;
            $album_id = $album_id['id'];
            unset($options['id']);
        }
        if(!isset($options['is_live'])) $options['is_live'] = '1';
        $options['_order_by'] = array('sequence'=>'asc');
        $options['album_id'] = $album_id;

        // load cache helper if it does not exist
        $CI = &get_instance();

        $cache_key = 'res/album/'.$album_id.'/is_live_'.$options['is_live'];

        $locale_code = '';

        if( $this->is_localized ){
            $locale_code = $CI->lang->locale();
            $cache_key .='/'.$locale_code;
        }

        if(!function_exists('cache_get')){
            $CI->load->helper('cache');
        }

        $cache_allowed = false;
        if( is_bool($load_cache)){
            $cache_allowed = $load_cache;
        } else{
            $cache_allowed = true;
            $cache_ttl = $load_cache;
        }

        $r = cache_get($cache_key);

        if(!$cache_allowed || empty($r)){
            $query = array('id'=>$album_id,'is_live'=>$options['is_live']);
            $r = $this ->album_model->read($query);
            if(empty($r['id'])){
                return NULL;
            }
            $photo_files = $this ->album_photo_model->find( $options );
            
            $r['photos'] = array();
            if(is_array($photo_files)){
                foreach($photo_files as $photo_info){
                    $file_id = $photo_info['main_file_id'];

                    // override file id for multiple language
                    if( $this->is_localized ){
                        if(!empty($photo_info['parameters']['loc'][ $locale_code  ]['file_id'])){
                            $file_id = $photo_info['parameters']['loc'][ $locale_code  ]['file_id'];
                        }
                    }

                    $file = $this->get_file($file_id, NULL, $cache_ttl);

                    $file['default_file_id'] = $photo_info['main_file_id'];
                    $file['parameters'] = $photo_info['parameters'];
                    if(isset($photo_info['is_live']))
                        $file['is_live'] = $photo_info['is_live'];
                    $file['relation_id'] = $photo_info['id'];

                    $r['photos'] [] = $file;
                }
            }
            if($cache_ttl>0){
                cache_set($cache_key, $r,$cache_ttl);
                log_message('debug', 'ResourceModule/get_album, saving cache for album '.$album_id.' by key '.$cache_key);
            }
        }else{
            log_message('debug', 'ResourceModule/get_album, getting cache for album '.$album_id.' by key '.$cache_key);
        }
        return $r;
    }

    public function reset_album_cache($r){
        $cache_key = 'res/album/'.$r['id'].'';

        // load cache helper if it does not exist
        $CI = &get_instance();
        if(!function_exists('cache_get')){
            $CI->load->helper('cache');
        }

        log_message('debug', 'ResourceModule/reset_album_cache, removing cache for album '.$r['id'].' by key '.$cache_key);
        cache_remove($cache_key);

    }
}
}