<?php 
namespace ACP\Controllers\Admin{

	use \MY_Controller;

	class CRUDController extends MY_Controller
	{
		// string
		var $model = 'target_model';
		var $tree = 'target';
		var $view_prefix = '';
		var $view_scope = 'target';
		var $view_type = 'post';
		var $page_header = 'target_heading';
		var $deep = 0;
		var $localized = FALSE;
		var $listing_fields = NULL;
		var $locale_fields = array('title','description','content','parameters','status');
		var $sorting_fields = array('priority','id','publish_date','create_date','modify_date');
		var $mapping_fields = NULL;
		var $editable_fields = NULL; // default all fields could be edited.
		var $editable_fields_details = array(); // default all fields could be edited.
		var $extra_fields = NULL;
		var $sorting_direction = 'desc';
		var $keyword_fields = array('id','title','content','description');
		var $cache_prefix = NULL;
		var $action = 'index';
		var $has_record_view = FALSE;
		var $home_related = FALSE;

		var $add_enabled = TRUE;
		var $edit_enabled = TRUE;
		var $remove_enabled = TRUE;

		var $view_path_prefix = NULL;
		var $view_paths = array(
			'index'=>'core/post_index',
			'add'=>'core/post_editor',
			'edit'=>'core/post_editor',
		);
		var $endpoint_path_prefix = NULL;

		var $batch_actions = array();
		var $staging_enabled = FALSE;
		var $export_enabled = FALSE;

		var $premission_key_prefix = '';

		var $default_page_limit = 50;

		var $_target_model = NULL;

		public function __construct(){
			parent::__construct();

			$this->_init();
		}

		protected function _init(){

			$model_name = $this->model;
			
			$this->load->model($model_name);
			$this->_target_model = $this->$model_name;

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('', '');

			

			if(empty($this->batch_actions)){
				if($this->remove_enabled)
					$this->batch_actions[ 'remove'] = 'remove';
				if($this->staging_enabled)
					$this->batch_actions[ 'publish'] = 'publish';
				if(in_array( 'status', $this->_target_model->fields )){
					$this->batch_actions[ 'status-enable'] = 'status_enable';
					$this->batch_actions[ 'status-disable'] = 'status_disable';
				}
			}

			$this->config->set_item('main_menu_selected', $this->tree);
			if($this->localized){
				$this->load->model('text_locale_model');
			}

			if($this->home_related){
				$this->load->model('home_content_model');
			}

			// if controller does not contain path prefix, combined by configured version
			if(empty($this->view_path_prefix)) 
				$this->view_path_prefix = $this->view_prefix.$this->view_scope.'/'.$this->view_type;

			// if controller does not contain path prefix, combined by configured version
			if(empty($this->endpoint_path_prefix)) 
				$this->endpoint_path_prefix = $this->view_prefix.$this->view_scope.'/'.$this->view_type;


			$this->editable_fields = array();
			if(!empty($this->_target_model->fields_details) && is_array($this->_target_model->fields_details)){
				foreach($this->_target_model->fields_details as $field_name => $field_info){
					if(!empty($field_info['control'])){
						$this->editable_fields[] = $field_name;
						$this->editable_fields_details[$field_name] = $field_info;
					}
				}
			}

			if(!empty($this->extra_fields) && is_array($this->extra_fields)){
				foreach($this->extra_fields as $field_name => $field_info){
					if(!empty($field_info['control'])){
						$this->editable_fields[] = $field_name;
						$this->editable_fields_details[$field_name] = $field_info;
					}
				}
			}
		}

		protected function _get_default_vals($action = 'index', $vals = array()){
			$vals = parent::_get_default_vals($action, $vals);
			if($action == 'search'){

				$vals['paging'] = array();
				$vals['paging']['offset'] = 0;
				$vals['paging']['total'] = 0;
				$vals['paging']['limit'] = 0;
				$vals['paging']['page'] = 0;
				$vals['paging']['total_page'] = 0;
				$vals['data'] = array();
			}
			return $vals;
		}

		protected function _shorten_text($val, $length=200, $tail = '...', $encoding = 'UTF-8'){
			$size = mb_strlen($val, $encoding);
			if($size > $length){
				return mb_substr($val, 0, $length - strlen($tail), $encoding);
			}
			return $val;
		}

		protected function _available_locales(){
			return $this->lang->get_available_locale_keys();
		}
		
		protected function _mapping_row($raw_row){
			$row = array();
			if(empty($raw_row['id'])) return NULL;

			if(!empty($this->mapping_fields) && is_array($this->mapping_fields)){
				foreach($this->mapping_fields as $field_name){
						$row[$field_name] = data($field_name, $raw_row);
				}
			}

			if(isset($raw_row['id']))
					$row['id'] = $raw_row['id'];

			if(isset($raw_row['slug'])){
				$row['slug'] = $raw_row['slug'];
			}
			if(isset($raw_row['priority'])){
				$row['priority'] = intval($raw_row['priority']);
			}

			// get localized content
			if($this->localized){
				$loc_options = array(
					'_field_based'=>'locale',
					'_select'=>'id,is_live,cover_id,locale,title,content,description,parameters,status',
					'ref_table'=>$this->_target_model->table,
					'ref_id'=>$raw_row['id'],
					'locale'=>$this->_available_locales(),
				);
				if(isset($raw_row['is_live'])) $loc_options['is_live'] = $raw_row['is_live'];

				$row['loc'] = $this->text_locale_model->find($loc_options);
				$cur_locale = $this->lang->locale();

				if(isset($row['loc'][$cur_locale])){
					$loc_data = $row['loc'][$cur_locale];

					if(isset($row['title'])){
						$row['raw_title'] = $row['title'];
					}
					if(isset($loc_data['title'])){
						$row['loc_title'] = $loc_data['title'];
					}
					if(isset($row['description'])){
						$row['raw_description'] = $row['description'];
					}
					if(isset($loc_data['description'])){
						$row['loc_description'] = $loc_data['description'];
					}
					if(isset($row['content'])){
						$row['raw_content'] = $row['content'];
					}
					if(isset($loc_data['content'])){
						$row['loc_content'] = $loc_data['content'];
					}
					if(isset($row['parameters'])){
						$row['raw_parameters'] = $row['parameters'];
					}
					if(isset($loc_data['parameters'])){
						$row['loc_parameters'] = $loc_data['parameters'];
					}
				}
			}

			if(in_array('title',$this->_target_model->fields) || in_array('title',$this->_target_model->fields_details)){
				$row['title'] = isset($raw_row['title']) ? $raw_row['title'] : '';
				
				if(isset($raw_row['loc_title'])){
					$row['title'] = $raw_row['loc_title'];
				}
			}
			
			if(in_array('description',$this->_target_model->fields) || in_array('description',$this->_target_model->fields_details)){
				$row['description'] = isset($raw_row['description']) ? $raw_row['description'] : '';

				if(isset($raw_row['loc_description'])){
					$row['description'] = $raw_row['loc_description'];
				}
				$row['description_short'] = $this->_shorten_text($row['description']);
			}

			if(in_array('content',$this->_target_model->fields) || in_array('content',$this->_target_model->fields_details)){
				$row['content'] = isset($raw_row['content']) ? $raw_row['content'] : '';

				if(isset($raw_row['loc_content'])){
					$row['content'] = $raw_row['loc_content'];
				}

				$row['content_short'] = $this->_shorten_text(strip_tags($row['content']));
			}
			
			$row['status'] = isset($raw_row['status']) ? $raw_row['status'] : '';
			
			if(!empty($row['status'])){
				$row['status_str'] = lang('status_'.$raw_row['status']);
			}
			if(in_array('publish_date',$this->_target_model->fields) || in_array('publish_date',$this->_target_model->fields_details)){
				$row['published'] = '';
				if(!empty($raw_row['publish_date'])){
					$row['published_ts'] = strtotime($raw_row['publish_date']);
				}
			}
			if(in_array('create_date',$this->_target_model->fields)){
				$row['created'] = '';
				if(!empty($raw_row['create_date'])){
					$row['create_ts'] = strtotime($raw_row['create_date']);
				}
			}
			if(in_array('modify_date',$this->_target_model->fields)){
				$row['modified'] = '';
				if(!empty($raw_row['modify_date'])){
					$row['modify_ts'] = strtotime($raw_row['modify_date']);
				}
			}

			if($this->staging_enabled){

				$row['is_live'] = isset($raw_row['is_live']) ? $raw_row['is_live'] : '';
				$row['is_live_str'] = lang('is_live_'.$row['is_live']);
				$row['is_pushed'] = isset($raw_row['is_pushed']) ? $raw_row['is_pushed'] : '';
				$row['is_pushed_str'] = lang('is_pushed_'.$row['is_pushed']);
				$row['last_pushed'] = isset($raw_row['last_pushed']) ? $raw_row['last_pushed'] : '';
				$row['last_pushed_ts'] = isset($raw_row['last_pushed']) ? strtotime($raw_row['last_pushed']) : '';
			}
			if(isset($raw_row['_mapping']))
				$row['_mapping'] = $raw_row['_mapping'];

			foreach($this->_target_model->fields_details as $field_name => $field_info){
				if(!isset($row[ $field_name ]) && isset($raw_row[$field_name]) && (isset($field_info['listing']) && $field_info['listing'] || isset($field_info['export']) && $field_info['export'])){
					$row[ $field_name ] = $raw_row[$field_name];
				}
			}

			return $row;
		}

		protected function _clear_cache($raw_row){
			if(!empty($this->cache_prefix) && !empty($raw_row['id'])){
				$cache_prefix = $this->cache_prefix;
				cache_remove($cache_prefix.'/'.$raw_row['id'].'/*');
				cache_remove($cache_prefix.'/'.$raw_row['id']);
				if(!empty($raw_row['_mapping'])){
					cache_remove($cache_prefix.'/'.$raw_row['_mapping'].'/*');
					cache_remove($cache_prefix.'/'.$raw_row['_mapping']);
				}
			}
		}

		protected function _segment_at($offset=0){
			$offset = $this->deep + 2 + $offset;
			return $this->uri->segment($offset);
		}
		
		public function _remap(){
			
			$s1 = $this->_segment_at(0);
			$s2 = $this->_segment_at(1);
			$s3 = $this->_segment_at(2);
			$s4 = $this->_segment_at(3);

			if( $this->_mapping_action($s1,$s2,$s3,$s4) ){
				return ;
			}

			if( $this->_is_record_id($s1) ){
				if( $this->_record_action($s1, $s2,$s3, $s4) ){
					return;
				}
			}
			
			return $this->_show_404('route_not_matched');
		}

		protected function _mapping_action($s1,$s2=NULL,$s3=NULL,$s4=NULL){
			
			if(in_array($s1, array('','index','selector'))){
				$this->action = $s1;
				$this->index();
				return TRUE;
			}
			if(in_array($s1, array('all'))){
				$this->action = $s1;
				$this->all();
				return TRUE;
			}
			if(in_array($s1, array('search'))){
				$this->action = $s1;
				$this->search();
				return TRUE;
			}
			if(in_array($s1, array('batch'))){
				$this->action = $s1;
				$this->batch($s2);
				return TRUE;
			}
			if(in_array($s1, array('save'))){
				$this->action = $s1;
				$this->save();
				return TRUE;
			}
			if(in_array($s1, array('export'))){
				$this->action = $s1;
				$this->export();
				return TRUE;
			}
			if(in_array($s1, array('remove','delete'))){
				$this->action = $s1;
				$this->delete();
				return TRUE;
			}
			if(in_array($s1, array('add'))){
				$this->action = $s1;
				$this->editor();
				return TRUE;
			}
			return FALSE;
		}

		protected function _is_record_id($str){
			$id_pattern = '/^[0-9]+$/';
			if($this->_target_model->use_guid){
				$id_pattern = '/^([a-zA-Z0-9-]+)$/';
			}

			return preg_match($id_pattern,$str);
		}

		protected function _record_action($id, $action=false, $action_id=NULL, $subaction=NULL){
			if(!$action || empty($action) ){
				$action = 'view';
			}

			if($action == 'edit'){
				$this->action = $action;
				$this->editor($id);
				return TRUE;
			}

			if( $action == 'view'){
				$this->action = $action;
				$this->view($id);
				return TRUE;
			}

			return FALSE;
		}

		protected function _get_permission_scopes($keys = NULL){
			if($keys === NULL) return NULL;
			$_keys = array();

			if(is_array($keys)){
				foreach($keys as $key)
					$_keys[] = $this->premission_key_prefix.$key;
			}else{
					$_keys[] = $this->premission_key_prefix.$keys;
			}
			return $_keys;
		}
		
		public function index(){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_LISTING'))){
				return;
			}

			$vals = $this->_get_default_vals('index');

			$view_path = $this->_get_render_view('index');
			
			if($this->uri->is_extension('js'))
				return $this->_render($view_path.'.js',$vals);
			
			$this->_render($view_path,$vals);
		}
		
		// default record's view
		public function view($record_id=false){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_VIEW'))){
				return;
			}


			$method = $this->input->request_method();

			$record = NULL;
			if(!empty($record_id)){
				$query_opts = $this->_select_options('editor', array('id'=>$record_id));
				$record = $this->_target_model->read($query_opts);
				if(empty($record['id'])){
					return $this->_show_404('record_not_found');
				}
				if($this->uri->is_extension($this->supported_data_extension) && strtoupper($method) == 'GET'){
					return $this->_api( $this->_mapping_row($record) );
				}
			}

			if($this->has_record_view){

				$vals = $this->_get_default_vals('view');
				$vals['record'] = $record;
				$vals['record_id'] = $record_id;

				$view_path = $this->_get_render_view('view');

				if($this->uri->is_extension('js'))
					return $this->_render($view_path.'.js',$vals);
				
				return $this->_render($view_path,$vals);

			}else{
				return redirect($this->endpoint_path_prefix.'/'.$record_id.'/edit');
			}

			return $this->_show_404('no_view');
		}
		
		public function editor($record_id=false){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_EDITOR'))){
				return;
			}
			
			$record = NULL;

			$vals = NULL;

			$action = 'add';
			
			if(!empty($record_id)){
				$query_opts = $this->_select_options('editor', array('id'=>$record_id));
				$record = $this->_target_model->read($query_opts);
				if(empty($record['id'])){
					return $this->_show_404('record_not_found');
				}
				$vals = $this->_get_default_vals('edit', compact('record','record_id'));
				$vals['id'] = $record_id;
				$vals['record_id'] = $record_id;
				$vals['record'] = $this->_mapping_row($record);
				$action = 'edit';

				if(!isset($vals['data'])){
					$vals['data'] = $record;
				}
			}else{
				$vals = $this->_get_default_vals('add');
				$vals['id'] = NULL;
				$vals['record_id'] = NULL;
				$vals['record'] = NULL;
				if(!isset($vals['data']))
					$vals['data'] = $this->_target_model->new_default_values();
			}

			if($action == 'add' && !$this->add_enabled){
				return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
			}

			if($action == 'edit' && !$this->edit_enabled){
				return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
			}
			
			// localized content
			$vals['loc'] = array();

			if($this->localized){
				if(!empty($record)){
					
					$vals['loc'] = $this->text_locale_model->find(array('ref_table'=>$this->_target_model->table,'ref_id'=>$record_id,'is_live'=>'0','_field_based'=>'locale'));

					$loc_fields = $this->locale_fields;

					foreach($this->lang->get_available_locale_keys() as $loc_key ){
						if(!isset($vals['loc'][$loc_key])){
							$vals['loc'][$loc_key]=array();
							$vals['loc'][$loc_key]['locale'] = $loc_key;

							foreach($loc_fields as $loc_field){
								if(isset($record[ $loc_field ])){
									$vals['loc'][$loc_key][$loc_field] = $record[$loc_field];
								}
							}
						}
					}
				}
			}

			$view_path = $this->_get_render_view($action);
			
			if($this->uri->is_extension('js'))
				return $this->_render($view_path.'.js',$vals);
			
			$this->_render($view_path,$vals);
		}

		public function publish($id=false,$return=false){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_PUBLISH'), !$return )){
				if(!$return)
					$this->_error(ERROR_INVALID_SESSION, 'Valid session required.');
				return FALSE;
			}

			if(is_array($id) && isset($id['id'])){
				$id = $id['id'];
			}
			
			if(empty($id)){
				$this->_error(ERROR_INVALID_DATA, 'Passed invalid value.');
				return;
			}
			
			$old_row = $this->_target_model->read(array('id'=>$id,'is_live'=>1));
			if(isset($old_row['id'])){

				$this->_clear_cache($old_row);
				$this->_target_model->delete(array('id'=>$id,'is_live'=>1));
			}
			$new_row = $this->_target_model->read(array('id'=>$id,'is_live'=>0));

			//log_message('debug','publish, new row:'.print_r($new_row,true));
			$new_row['is_live'] = 1;

			$result = $this->_target_model->save($new_row);

			if($this->localized){
				$all_curr_loc_rows = $this->text_locale_model->find(array(
					'is_live'=>'0',
					'ref_table'=>$this->_target_model->table, 
					'ref_id'=>$id,
					'_field_based'=>'locale'
					));

				$this->text_locale_model->delete(array(
					'is_live'=>'1',
					'ref_table'=>$this->_target_model->table, 
					'ref_id'=>$id
					));

				foreach($all_curr_loc_rows as $loc_code => $loc_data){
					$loc_data['is_live'] = '1';
					$this->text_locale_model->save($loc_data);
				}
			}

			if(isset($result['id'])){
				$this->_after_publish($result['id']);
			}
			
			if($return){
				
				if(isset($result['id'])){
					if($result['id'] == $id){
						return array('id'=>$id);
					}
					return array('id'=>$id,'error'=>array('code'=>ERROR_INVALID_DATA, 'message'=>'Invalid id after save live content.'));
				}
				return array('id'=>$id,'error'=>array('code'=>ERROR_RECORD_SAVE_ERROR, 'message'=>'Cannot save record in database.'));
			}
			
			if(isset($result['id'])){
				if($result['id'] == $id){
					return $this->_api(array('id'=>$id));
				}
				return $this->_error(ERROR_INVALID_DATA, 'Invalid id after save live content.');
			}
			return $this->_error(ERROR_RECORD_SAVE_ERROR, 'Cannot save record in database.');
		}

		protected function _after_publish($record_id){

			
			// update attributes
			$this->_target_model->save(array('is_pushed'=>1, 'last_pushed'=>time_to_date()),array('id'=>$record_id));
		}


		protected function _search_options($options=false){

			if(!$options) $options = array();

			$direction = $this->sorting_direction;
			$sort = $this->sorting_fields[0];

			$start = (isset($options['_paging_offset'])) ? $options['_paging_offset'] : 0 ;
			$limit = (isset($options['_paging_limit'])) ? $options['_paging_limit'] : $this->default_page_limit;
			
			if($this->input->get_post('direction')!==false){
				$direction = $this->input->get_post('direction');
				if(strtolower($direction) != 'desc') $direction = 'asc';
			}
			if($this->input->get_post('sort')!==false){
				$sort = $this->input->get_post('sort');

				if(!in_array($sort,$this->sorting_fields)) $sort = $this->sorting_fields[0];
			}
			
			if($this->input->get_post('offset')!==NULL){
				$start = $this->input->get_post('offset');
			}
			if($this->input->get_post('limit')!==NULL){
				$limit = $this->input->get_post('limit');
			}
				
			if($this->input->get_post('page')!==NULL){
				$start = ($this->input->get_post('page') - 1 ) * $limit;
			}
			
			if($this->input->get_post('q')!=false && $this->input->get_post('q')!=''){
				$options['_keyword'] = $this->input->get_post('q');
				$options['_keyword_fields'] = $this->keyword_fields;
			}
			
			if($this->home_related){
				if($this->input->get('home_visible')!=''){
					$options['home_visible'] = $this->input->get('home_visible');
				}
			}
			
			if($start<0) $start = 0;
			if($limit < 5) $limit = 10;

				
			if($this->input->get_post('id')!==NULL){
				$options['id'] = $this->input->get_post('id');
			}
			
			
			$options['_order_by'] = array();


			$sorts = $this->input->get_post('sorts');
			if(!empty($sorts) && is_array($sorts)){
				foreach($sorts as $key => $_direction){
					if(in_array($key, $this->sorting_fields))
						$options['_order_by'][ $key ] = $_direction;
				}
			}

			if(empty($options['_order_by'][$sort]))
				$options['_order_by'][$sort] = $direction;

			if( in_array('create_date',$this->_target_model->fields))
				$options['_order_by'][ $this->_target_model->table.'.create_date' ] = $direction;

			if($this->localized){
				$options['_with_text'] = $this->lang->locale();
			}

			if($this->staging_enabled){
				$options['is_live'] = '0';
			}

			$options['_paging_start'] = $start;
			$options['_paging_limit'] = $limit;

			return $options;
		}

		protected function _select_options($action='default',$options=array()){

			if($this->staging_enabled){
				$options['is_live'] = '0';
			}

			return $options;
		}
		
		public function all(){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_LISTING'))){
				return;
			}
			
			if(!$this->_is_ext('data')){ 
				return $this->_show_404('extension_not_allowed');
			}
			
			$options = $this->_search_options();
			$req_limit = $this->input->get_post('limit');

			$options['_paging_limit'] =  $this->_target_model->get_total($options,false);
			
			$result = $this->_target_model->find_paged($options['_paging_start'],$options['_paging_limit'],$options,false);
			
			$vals = $this->_get_default_vals('search' );
			
				$vals['paging']['offset'] = 0;
				$vals['paging']['total'] = 0;
				$vals['paging']['limit'] = 0;
				$vals['paging']['page'] = 0;
				$vals['paging']['total_page'] = 0;
				$vals['data'] = array();
			
			if(isset($result['data'])){
				
				foreach($result['data'] as $idx => $row){
					$new_row = $this->_mapping_row($row);
					$new_row['_index'] = $result['index_from']  + $idx;
					$vals['data'][] = $new_row;
				}
				
				$vals['paging']['offset'] = $result['index_from'];
				$vals['paging']['total'] = $result['total_record'];
				$vals['paging']['limit'] = $result['limit'];
				$vals['paging']['page'] = $result['page'];
				$vals['paging']['total_page'] = $result['total_page'];
			}

			return $this->_api($vals);
		}
		
		public function search(){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_LISTING'))){
				return;
			}
			
			if(!$this->_is_ext('data')){ 
				return $this->_show_404('extension_not_allowed');
			}
			
			$options = $this->_search_options();
			$req_limit = $this->input->get_post('limit');

			if(!empty($req_limit) && intval($req_limit) < 0){
				$options['_paging_limit'] =  $this->_target_model->get_total($options,false);
			}
			
			$result = $this->_target_model->find_paged($options['_paging_start'],$options['_paging_limit'],$options,false);
			
			$vals = $this->_get_default_vals('search' );
			
				$vals['paging']['offset'] = 0;
				$vals['paging']['total'] = 0;
				$vals['paging']['limit'] = 0;
				$vals['paging']['page'] = 0;
				$vals['paging']['total_page'] = 0;
				$vals['data'] = array();
			
			if(isset($result['data'])){
				
				foreach($result['data'] as $idx => $row){
					$new_row = $this->_mapping_row($row);
					$new_row['_index'] = $result['index_from']  + $idx;
					$vals['data'][] = $new_row;
				}
				
				$vals['paging']['offset'] = $result['index_from'];
				$vals['paging']['total'] = $result['total_record'];
				$vals['paging']['limit'] = $result['limit'];
				$vals['paging']['page'] = $result['page'];
				$vals['paging']['total_page'] = $result['total_page'];
			}

			return $this->_api($vals);
		}

		public function export(){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_LISTING'))){
				return;
			}
			if( $this->_restrict($this->_get_permission_scopes('RECORD_EXPORT'))){
				return;
			}

			if( !$this->export_enabled){
				return $this->_show_404('action_not_enabled');
			}

			if(!class_exists('PHPExcel')){
				return $this->_error(-1 ,'Library "PHPExcel" is missing.');
			}

			$vals = $this->_get_default_vals('export');

			$options = $this->_select_options('export');

			$records = $this->_target_model->find($options);
			$fields = array();

			foreach($this->_target_model->fields_details as $field_name => $field_info){
				if((!empty($field_info['listing']) && $field_info['listing']) || (!empty($field_info['export']) && $field_info['export'])){
					$fields[] = $field_name;
				}
			}

			$req_limit = $this->input->get_post('limit');

			// get the search result
			$options = $this->_search_options(array('_paging_limit' => $req_limit));

			if(empty($req_limit)){
				$options['_paging_limit'] =  $this->_target_model->get_total($options,false);
			}

			
			$result = $this->_target_model->find_paged($options['_paging_start'],$options['_paging_limit'],$options,false);
			
			$data = array();
			$fields = $this->mapping_fields;

			if(isset($result['data']) && is_array($result['data'])){
				foreach($result['data'] as $idx => $row){
					$new_row = $this->_mapping_row($row);
					$data[ ] = $new_row;
					if(empty($fields)) $fields = array_keys($new_row);
				}
			}



			$format = $this->input->get_post('format');

			if(in_array($format, array( NULL, '', 'xls','xlsx'))){


				$excel = new PHPExcel();
				$sheet = $excel->getActiveSheet();

				foreach ($fields as $idx => $field_name) {
					$columnID = chr( 65 + $idx );
					$label = lang('field_'. $field_name);

					if(isset($this->extra_fields[ $field_name]['label']))
						$label = $this->extra_fields[ $field_name]['label'];

					$sheet->setCellValue( $columnID . '1', $label);

				    $sheet->getColumnDimension($columnID)
				        ->setAutoSize(true);
				}

				$num_total = count($data);
				for($i = 0; $i< $num_total; $i++){
					$row = $data[$i];

					foreach ($fields as $idx => $field_name) {
						$columnID = chr( 65 + $idx );
						$sheet->setCellValue($columnID .($i+2), data($field_name,$row ) );
		
					}
				}

				$cache_dir = $this->config->item('cache_path');
				$file_name = $this->view_scope . '_'.$this->view_type . '-'.date('YmdHis');

				if($format == 'xlsx'){
					$objWriter = new PHPExcel_Writer_Excel2007($excel);
					$file_fullname = $file_name.'.xlsx';
				}else{
					$objWriter = new PHPExcel_Writer_Excel5($excel);
					$file_fullname = $file_name.'.xls';
				}
				$file_path = $cache_dir.$file_fullname;
				$objWriter->save($file_path);

				if(!file_exists($file_path)){
					return $this->_error('No file output.');
				}

				$this->load->helper('download');
				force_download($file_fullname, file_get_contents($file_path));

				return;
			}
			if(in_array($format, array('','json'))){
				$this->_api(compact('data'));
				return;
			}
			return $this->_show_404();
		}

		public function batch($action=''){
			if($this->_restrict()){
				return ;
			}
			
			$ids = $this->input->get_post('ids');
			$ids = explode(",", trim($ids));
			if(!is_array($ids)){
				return $this->_error(ERROR_INVALID_DATA, lang(ERROR_INVALID_DATA_MSG));
			}
			
			$query_opts = $this->_select_options('batch', array('id'=>$ids));

			$records = $this->_target_model->find($query_opts);
			if(is_array($records) && count($records)>0){
				foreach($records as $idx => $record){
					
					$result[ $record['id'] ] = $this->_batch_action($action, $record);
				}
				
				return $this->_api(array('result'=>$result,'data'=>$ids));
			}else{
				return $this->_error(ERROR_NO_RECORD_LOADED,lang(ERROR_NO_RECORD_LOADED_MSG));
			}
		}

		protected function _batch_action($action, $record){
			if(isset($this->batch_actions[ $action])){
				$method = $this->batch_actions[ $action];
				if(method_exists($this, $method)){
					$this->_clear_cache($record);
					return $this->$method($record, TRUE); // second parameter should be return for TRUE
				}
			}
			return FALSE;
		}

		public function status_enable($record){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_PROPERTY_CHANGE'), FALSE)){
				return FALSE;
			}
			
			$query_opts = $this->_select_options('status_disable', array('id'=>$record['id']));
			$this->_target_model->save(array('status'=>1), $query_opts);

			return TRUE;
		}

		public function status_disable($record){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_PROPERTY_CHANGE'), FALSE)){
				return FALSE;
			}
			
			$query_opts = $this->_select_options('status_enable', array('id'=>$record['id']));
			$this->_target_model->save(array('status'=>0), $query_opts);

			return TRUE;
		}
		
		public function save($id=false){
			
			if( $this->_restrict($this->_get_permission_scopes('RECORD_SAVE'))){
				return FALSE;
			}

			$editor_info =$this->_get_editor_info();
			
			$vals = $this->_get_default_vals('save');
			$success = true;
			
			if(!$id) $id = $this->input->get_post('id');
			
			$query_opts = $this->_select_options('save', array('id'=>$id));

			$record = $old_record = NULL;
			if(!empty($id)){
				$old_record = $record =$this->_target_model->read($query_opts) ;
				if(!isset($record['id']) || $record['id'] != $id){
					return $this->_show_404('record_id_not_matched');
				}
			}

			$action = !$id ? 'add' : 'edit';
			$data = array();
			$loc_data = array();

			if($action == 'add' && !$this->add_enabled){
				return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
			}

			if($action == 'edir' && !$this->edit_enabled){
				return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
			}

			$success = $this->_before_save($action, $record, $data, $loc_data);
			
			if($success != FALSE)
				$success = TRUE;

			if($success){


				//$locale = $this->lang->locale();


				$validate = array();
				if(!$this->_validate_save($query_opts, $old_record, $data, $validate)) {
					return $this->_error(ERROR_RECORD_VALIDATION, lang(ERROR_RECORD_VALIDATION_MSG),'200', compact('validate'));
				}

				if(!$id){
					if($this->staging_enabled)
						$data['is_live'] = '0';
					$result = $this->_target_model->save($data,NULL, $editor_info);
					$query_opts = $this->_select_options('save', array('id'=>$result['id']));
					$id = $result['id'];
				}else{
					unset($data['is_live']);
					unset($data['id']);
					unset($data['create_date']);
					unset($data['create_by']);
					unset($data['create_by_id']);
					$result = $this->_target_model->save($data,$query_opts, $editor_info);
				}

				$record = $this->_target_model->read($query_opts);

				$this->_after_save($action, $id, $old_record, $data, $loc_data, $vals);

				
				$vals['id'] = $id;
				$vals['method'] =$action;
				$vals['data'] = array();
				foreach($this->editable_fields as $idx => $field_name)
					$vals['data'] [ $field_name ] = data($field_name, $record);

				if(isset($this->db) && $this->_is_debug())
					$vals['queries'] = $this->db->queries;

				$this->_clear_cache($record);
			}
			
			if($this->uri->is_extension('')){
				redirect($this->view_prefix.$this->view_scope.'/'.$this->view_type.'/'.$id);
				return;
			}
			if($this->_is_ext('data')){
				return $this->_api($vals);
			}
			return $this->_show_404();
		}

		protected function _before_save($action, $record, &$data=false, &$loc_data=false ){

			$defvals = $this->_target_model->new_default_values();

			foreach($defvals as $field=>$val){

				// Ignore fields if the default values does not allowed for save action
				if(!empty($this->editable_fields)){
					if(!in_array($field, $this->editable_fields))
						continue;
				}
				$post_value = $this->input->post($field);

				if(!isset($data[$field]))
					$data[$field] = $val;
				if(isset($record[$field])) 
					$data[$field] = $record[$field];
				if($post_value !== NULL && $post_value !== FALSE) 
					$data[$field] = $post_value ;
			}
			
			if(isset($data['publish_date'])){
				if((substr($data['publish_date'],0,4) == '0000' || empty($data['publish_date'])) && $data['status'] == '1'){
					$data['publish_date'] = date('Y-m-d H:i:s');
				}
			}

			if($this->localized){
				// prepare data for localized content
				$loc_data = $this->input->post('loc');
				$default_locale = $this->input->post('default_locale');
				$locale = $this->lang->locale();

				// only this fields will be handled for localized
				$locale_fields = $this->locale_fields;

				if(empty($default_locale)) $data['default_locale'] = $default_locale = $this->lang->locale();

				$_loc_data = isset($loc_data[$default_locale]) ? $loc_data[$default_locale] : NULL;

				$sql_loc_data = array();
				foreach($locale_fields as $idx => $field_name){
					if(isset($_loc_data[$field_name]))
						$data [$field_name] = $_loc_data[$field_name];
				}

				if(empty($loc_data)){
					foreach($this->lang->get_available_locale_keys() as $loc_code){
						$_loc_data_row = array();
						foreach($locale_fields as $idx => $field_name){
							if(isset( $data[$field_name]))
							$_loc_data_row[$field_name] = $data[$field_name];
						}
						$loc_data[$loc_code] = $_loc_data_row;
					}
				}
			}
			return TRUE;
		}

		protected function _validate_save($query_options, $old_record, $data, &$validate = NULL){
			$success = TRUE;

			$validate_fields = array();
			if(!empty($this->editable_fields_details) && is_array($this->editable_fields_details)){
				foreach($this->editable_fields_details as $field_name => $field_info){

					if(!empty($field_info['validate'])){
						$validate_fields [] = $field_name;
						$_POST[$field_name] = data($field_name, $data);
						$this->form_validation->set_rules($field_name, ('lang:field_'.$field_name), $field_info['validate']);
					}
				}	
			}

			if(empty($validate_fields)) return TRUE;
			
			$success = $this->form_validation->run() != FALSE;

			foreach($validate_fields as $field_name){
				$validate['data'][ $field_name] = data($field_name, $data);

				$msg = $this->form_validation->error($field_name);
				if(!empty($msg)){
					$validate['fields'][ $field_name] = $msg;
				}
			}

			return $success;
		}

		protected function _after_save($action, $id, $old_record, $data, $loc_data, &$vals = false){
			$editor_info = $this->_get_editor_info();
			
			// localized part
			if($this->localized){

				// required helper and models
				$this->load->helper('localized');

				foreach($this->lang->get_available_locale_keys() as $loc_code){
					$_loc_data = isset($loc_data[$loc_code]) ? $loc_data[$loc_code] : NULL;

					// skip it if no data for this locale
					if(empty($_loc_data)) continue;
					localized_save($this->_target_model->table, $id, $loc_code, $_loc_data,'0',$editor_info);
				}
			}

			if($this->home_related){
				// update home visible content
				if($record['home_visible'] == '1'){
					$home_content_row = $this->home_content_model-> read( array(
						'ref_scope' => $this->_target_model->table,
						'ref_id'=>$record['id'],
					));
					if(!isset($home_content_row['id'])){
						$this->home_content_model->save(array(
							'ref_scope'=> $this->_target_model->table,
							'ref_id'=>$record['id'],
							'ref_slug'=>isset($record['slug']) ? $record['slug'] : NULL,
							'cover_id'=>isset($record['cover_id']) ? $record['cover_id'] : NULL,
							'title'=>$record['title'],
							'description'=>$record['description'],
							'publish_date'=>$record['publish_date'],
							'status'=>$record['status'],
						));
					}else{
						$this->home_content_model->save(array(
							'ref_slug'=>isset($record['slug']) ? $record['slug'] : NULL,
							'cover_id'=>isset($record['cover_id']) ? $record['cover_id'] : NULL,
							'title'=>$record['title'],
							'description'=>$record['description'],
							'publish_date'=>$record['publish_date'],
							'status'=>$record['status'],
						),array(
							'id'=>$home_content_row['id'],
						));
					}
				}else{
					$this->home_content_model-> delete( array(
						'ref_scope' => $this->_target_model->table,
						'ref_id'=>$record['id'],
					));
				}
				cache_remove('home_contents');
			}
		}

		public function remove($record){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_DELETE'), FALSE )){
				return ERROR_MISSING_PERMISSION;
			}

			if(!$this->remove_enabled){
				return ERROR_MISSING_PERMISSION_MSG;
			}
			

					
			$query_opts = $this->_select_options('delete', array('id'=>$record['id']));
			$this->_target_model->delete($query_opts);

			$this->_after_delete($record['id'], $record);

			$this->_clear_cache($record);

			return TRUE;
		}

		public function delete(){
			if( $this->_restrict($this->_get_permission_scopes('RECORD_DELETE'))){
				return ;
			}

			if(!$this->remove_enabled){
				return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
			}
			
			$ids = $this->input->post('ids');
			$ids = explode(",", trim($ids));
			if(!is_array($ids)){
				return $this->_error(ERROR_INVALID_DATA, lang(ERROR_INVALID_DATA_MSG));
			}
			
			$query_opts = $this->_select_options('delete', array('id'=>$ids));

			$records = $this->_target_model->find($query_opts);
			if(is_array($records) && count($records)>0){
				foreach($records as $idx => $row){
					
					$query_opts = $this->_select_options('delete', array('id'=>$row['id']));
					$this->_target_model->delete($query_opts);

					$this->_after_delete($row['id'], $row);

					$this->_clear_cache($row);
				}
				
				
				return $this->_api(array('data'=>$ids));
			}else{
				return $this->_error(ERROR_NO_RECORD_LOADED, lang(ERROR_NO_RECORD_LOADED_MSG));
			}
		}

		protected function _after_delete($id, $record){

			$ref_queries = array('ref_scope'=>$this->_target_model->table,'ref_id'=>$record['id']);
			if($this->localized){
				// remove multiple language record
				$this->text_locale_model-> delete($ref_queries);
			}

			if($this->home_related){
				$this->home_content_model-> delete( $ref_queries );
			}
		}

		protected function _get_render_view($action = 'index'){

			// if controller does not contain path prefix, combined by configured version
			if(is_array($this->view_paths) && !empty($this->view_paths[$action])) 
				return $this->view_paths[$action];
			if($action == 'add' || $action == 'edit')
				return $this->view_path_prefix .'_editor';
			return $this->view_path_prefix .'_'.$action;
		}

		protected function _render($view, $vals = false, $layout = false, $theme = false){

			if(!$vals) $vals = array();

			$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>', '</strong></div>');


			$vals['target_model'] = $this->_target_model;

			$vals['add_enabled'] = $this->add_enabled;
			$vals['edit_enabled'] = $this->edit_enabled;
			$vals['remove_enabled'] = $this->remove_enabled;
			$vals['staging_enabled'] = $this->staging_enabled;
			$vals['batch_actions'] = $this->batch_actions;

			$vals['export_enabled'] = $this->export_enabled;

			$vals['editable_fields'] = $this->editable_fields;
			$vals['editable_fields_details'] = $this->editable_fields_details;
			$vals['mapping_fields'] = $this->mapping_fields;
			$vals['extra_fields'] = $this->extra_fields;

			$vals['listing_fields'] = $this->listing_fields;
			$vals['sorting_fields'] = $this->sorting_fields;
			$vals['sorting_direction'] = $this->sorting_direction;
			$vals['keyword_fields'] = $this->keyword_fields;

			// if passed data does not contain path prefix...
			if(empty($vals['view_path_prefix'])) 
				$vals['view_path_prefix'] = $this->view_path_prefix;

			// if controller does not contain path prefix, combined by configured version
			if(empty($vals['view_path_prefix']) || !is_string($vals['view_path_prefix'])) 
				$vals['view_path_prefix'] = $this->view_prefix.$this->view_scope.'/'.$this->view_type;

			// if passed data does not contain path prefix...
			if(empty($vals['endpoint_path_prefix'])) 
				$vals['endpoint_path_prefix'] = $this->endpoint_path_prefix;

			// if controller does not contain path prefix, combined by configured version
			if(empty($vals['endpoint_path_prefix'])) 
				$vals['endpoint_path_prefix'] = $this->view_prefix.$this->view_scope.'/'.$this->view_type;
			$vals['endpoint_url_prefix'] = site_url($vals['endpoint_path_prefix']);
			
			$vals['view_prefix'] = $this->view_prefix;
			$vals['view_scope'] = $this->view_scope;
			$vals['view_type'] = $this->view_type;

			if(!isset($vals['page_header']))
				$vals['page_header'] = lang($this->page_header);

			return parent::_render($view, $vals, $layout, $theme);
		}
	}
}