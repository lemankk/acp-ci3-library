<?php 
namespace ACP\Core{
use \CI_Controller;
use \CI_Model;
use \CI_Exceptions;

class HC_Controller extends CI_Controller
{
	var $request;
	var $response;
	var $resource;

	function __construct(){

		if(!isset($_SERVER['REMOTE_ADDR']))
			$_SERVER['REMOTE_ADDR'] = '0.0.0.0';

		parent::__construct();

		// load RequestModule for HttpRequest
		$this->request = new \ACP\Modules\RequestModule();

		// load ResponseModule for HttpResponse
		$this->response = new \ACP\Modules\ResponseModule();

		// load ResourceModule for handling file resource for output
		$this->resource = new \ACP\Modules\ResourceModule();

		// load ResourceModule for html output;
		$this->asset = new \ACP\Modules\AssetModule();

		$this->load->helper('render');
		
		$this->_init_session();

		$this->_init_system_setting();
	}

	protected function _init_system_setting(){

		$this -> load -> model('pref_model');

		// grap the system upload max size
		$sys_upload_max_size = ini_get('upload_max_filesize');
		if(strtoupper(substr($sys_upload_max_size, -1,1))=='M'){
			$sys_upload_max_size = intval(substr( $sys_upload_max_size,0, strlen($sys_upload_max_size)-1 ));
		}
		$this->config->set_item('sys_upload_max_size', $sys_upload_max_size);

		// get time zone setting
		$pref_timezone = $this->pref_model->item('timezone');

		if(!empty($pref_timezone)){
			$this->config->set_item('timezone', $pref_timezone);

			if(function_exists('date_default_timezone_set')){
				date_default_timezone_set($pref_timezone);	
			}
		}else{
			$pref_timezone = date_default_timezone_get();	
			$this->config->set_item('timezone', $pref_timezone);
		}

		if(isset($this->lang)){
			if($this->lang->default_locale_supported === FALSE){
				return $this->_show_404('locale_not_supported');
			}
		}
	}

	protected function _get_default_vals($action='index', $vals= array()){
		//$vals ['is_debug'] = $this->_is_debug();
		return $vals;
	}

	protected function _init_session(){
		$this->load->library('Session');
	}
	
	// return FALSE which is Allowed.
	public function _restrict($scope = NULL,$redirect=true){
		return FALSE;
	}
	
	public function _permission_denied($scope=NULL){
		return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG, 401, compact('scope'));
	}
	
	public function _is_debug(){
		return $this->request->is_debug();
	}

	public function _is_ext($group='html'){
		return $this->request->is_support_format($group);
	}

	public function _is_extension($group='html'){
		return $this->request->is_support_format($group);
	}

	public function _system_error($code, $message = 'Unknown system error.', $status=200, $data = NULL){
		return $this->response->system_error($code, $message, $status, $data);
	}
	
	public function _api($vals, $default_format = 'json') {
		return $this->response->output($vals, $default_format); 
	}
	
	public function _error($code, $message = '', $status = 200, $data=NULL) {
		return $this->response->error($code, $message, $status, $data); 
	}

	public function _show_404($message = 'unknown') {
		return $this->response->error404($message);
	}

	protected function _render($view, $vals = false, $layout = false, $theme = false) {
		return $this->response->view($view, $vals, $layout, $theme);
	}
}
}
