<?php
namespace ACP\Core{
class HC_Module
{

	function __construct(){

        $this -> load -> helper('datetime');
        $this -> load -> helper('data');
        $this -> load -> helper('render');
	}

	function __get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
    
    public function is_debug(){
        if($this->input->get('debug') == 'no'){
            return FALSE;
        }
        if ( ENVIRONMENT == 'development' && isset($this->config) && $this->config->item('debug_mode') == 'yes') {
            return TRUE;
        }
        if( defined('PROJECT_DEBUG_KEY') && $this->input->get('debug') == PROJECT_DEBUG_KEY)
            return TRUE;
        
        return FALSE;
    }

    public function system_error($code, $message = 'Unknown system error.', $status=200, $data = NULL){
        $case_id = uniqid('SER-');

        $vals = array(
            'uri_string'=>uri_string(),
            'uri_query'=>uri_query(),
            'post'=>$_POST,
            'get'=>$_GET,
            'data'=>$data,
        );

        // Log data into system folder.
        error_log('ErrorReport['.$case_id.'] info: '.print_r($vals, true));

        $message = '[Case ID: '.$case_id.']<br />'.$message;


        $data['case_id'] = $case_id;

        return $this->_error($code, $message, $status, $data);
    }

    public function error($code, $message = '', $status = 200, $data=NULL) {
        throw new \ACP\Core\HC_Exception ($code, $message, $data);
    }

    public function output($vals){
        return $vals;
    }
}
}