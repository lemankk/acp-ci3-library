<?php

$lang['member_login'] = '登入';
$lang['member_logout'] = '登出';

$lang['member_already'] = '已成為會員?';

$lang['member_signin'] = '登入';
$lang['member_signin_now'] = '立即登入';
$lang['member_signout'] = '登出';

$lang['member_profile'] = '個人檔案';
$lang['member_register'] = '加入我們';
$lang['member_register_now'] = '成為會員';
$lang['member_register_button'] = '立即登記';
$lang['member_agree'] = '同意';

$lang['member_change_password'] = '更換密碼';
$lang['member_reset_password'] = '重設密碼';


$lang['profile_loginid'] = '登入 ID';

$lang['profile_username'] = '登入名稱';
$lang['profile_username_description'] = '第一個字必須是英文字母，其餘 5 至 23 位 則可用英文及數字組成';
$lang['profile_username_error_empty'] = '請輸入你的登入名稱';
$lang['profile_username_error_used'] = '此登入名稱已被使用。如果你是此登入名稱的擁有者，你可以直嘗試重設密碼';
$lang['profile_username_error_invalid'] = '格式不正確';


$lang['profile_last_name'] = '名';

$lang['profile_first_name'] = '姓';

$lang['profile_phone'] = '電話';

$lang['profile_email'] = '電郵地址';
$lang['profile_email_description'] = '所有通訊及確認信件址會寄到此電郵地址中，請務必填寫有效的電郵地址';
$lang['profile_email_error_empty'] = '請輸入你的電郵地址';
$lang['profile_email_error_used'] = '此電郵地址已被使用。如果你是此登入名稱的擁有者，你可以直嘗試重設密碼';
$lang['profile_email_error_invalid'] = '格式不正確';

$lang['profile_email_confirm'] = '重填電郵地址';
$lang['profile_email_confirm_description'] = '請再次輸入你的電郵地址';
$lang['profile_email_confirm_error_notmatch'] = '重填電郵地址並不正確';


$lang['profile_password'] = '密碼';
$lang['profile_password_new'] = '新密碼';
$lang['profile_password_old'] = '舊密碼';
$lang['profile_password_old_incorrect'] = '舊密碼不正確';
$lang['profile_password_current'] = '現時密碼';
$lang['profile_password_current_description'] = '輸入你現時的密碼';
$lang['profile_password_change'] = '更改密碼';
$lang['profile_password_change_success'] = '更改密碼成功';
$lang['profile_password_change_fail'] = '更改密碼失敗';
$lang['profile_password_description'] =  '你的密碼應該是 6 至 24 字體長度, 包括文字或數字';
$lang['profile_password_error_empty'] = '請輸入你的密碼';
$lang['profile_password_error_invalid'] = '格式不正確';
$lang['profile_password_error_same'] = '新密碼不能與舊密碼相同';

$lang['profile_password_confirm'] = '重填密碼';
$lang['profile_password_confirm_description'] = '請再次輸入你的密碼';
$lang['profile_password_confirm_error_notmatch'] = '重填密碼並不正確';

$lang['securecode'] = '認證碼';
$lang['securecode_description'] = '請完整輸入以下顯示的英文字母及數字';
$lang['securecode_error_empty'] = '請輸入認證碼';
$lang['securecode_error_incorrect'] = '認證碼不正確。請重新輸入';
$lang['securecode_refresh_link'] = '無法看清？按此更換另一組認證碼?';

$lang['profile_agree_error_empty'] = '你必須同意才可以繼續.';

$lang['profile_privacy_link'] = '收集私隱資料條款';
$lang['profile_privacy_agress_link'] = '我同意並細閱 %s收集私隱資料條款%s';

$lang['profile_agree_option_yes'] = '是，我同意';
$lang['profile_agree_option_no'] = ' 否，我不同意';
$lang['profile_agree_description'] = '如果你選擇不同意，你將會無法繼續';

$lang['profile_subscribe'] = '我想接收任何推廣電郵、會員優惠通訊';

$lang['profile_full_name'] = '真實姓名';
$lang['profile_display_name'] = '別名';

$lang['profile_company'] = '公司名稱';
$lang['profile_building'] = '地住';
$lang['profile_street'] = ' 街到';
$lang['profile_city'] = '市';
$lang['profile_distrcit'] = '區域';
$lang['profile_postalcode'] = '郵寄編號';
$lang['profile_country'] = '國家或地區';

$lang['member_register_form_remark'] = '請填寫以下登記表格';
$lang['member_register_complete'] = '已登記完成';
$lang['member_register_submit'] = '立即登記';

$lang['profile_date_of_birth'] = '出生日期';
$lang['profile_date_of_birth_month'] = '月';
$lang['profile_date_of_birth_year'] = '年';
$lang['profile_date_of_birth_notprovide'] = '未選擇';

$lang['member_change_password_description'] = '你可以更換現有的登入密碼。請輸入以下所有資料。';

$lang['profile_current_password'] = '現有密碼';
$lang['profile_current_password_error_empty'] = '請輸入你的現有密碼';
$lang['profile_current_password_error_incorrect'] = '此密碼不正確';

$lang['profile_new_password'] = '新密碼';
$lang['profile_new_password_error_empty'] = '請輸入你的新密碼';

$lang['profile_new_password_confirm'] = '重填新密碼';

$lang['member_signin_required'] = '你必須登入才可以繼續使用我們的服務';

$lang['member_signin_description'] = '請先登入';

$lang['member_signin_welcome'] = '歡迎來到 UNKNOWN';
$lang['member_have_problem'] = '有問題?';
$lang['member_not_registered'] = '未成為會員?';

$lang['member_signin_error_unknown'] = '對不起，我們暫時無法處理你的要求。請稍後再試。';
$lang['member_signin_error_notfound'] = '對不起，我們無法找到你的戶口。';
$lang['member_signin_error_notverify'] = '你的戶口仍未啟用';

$lang['member_verify_link_message'] = '按此重新寄出啟用信件';
$lang['member_signin_password_error_incorrect'] = '密碼不正確';

$lang['member_verify_resend_success'] = '我們已經重新寄出確認電郵。請到你的電郵信箱等侯我們的信件（約48小時內）';
$lang['member_verify_resend'] = '重寄確認電郵';

$lang['member_verify_resend_description'] = '請輸入你已登記的電郵地址';

$lang['profile_email_error_not_found'] = '電郵地址並未登記';
$lang['profile_email_error_verified'] = '電郵地址已被確認';

$lang['member_profile_welcome'] = 'Yo! ';
$lang['member_profile_logged_as'] = '你已登入為 %s';

$lang['member_profile_edit'] = '更改戶口';
$lang['order_history'] = '購物紀錄';
$lang['member_profile_other_option'] ='其他選項';

$lang['membership_view'] = '會員身份';
?>