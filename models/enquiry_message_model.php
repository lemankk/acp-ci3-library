<?php
class Enquiry_message_model extends \ACP\Core\HC_Model {
	var $table  = 'enquiry_messages';
	var $use_guid = TRUE;

	var $fields_details = array(
		'id'		=> array(
			'type'  => 'VARCHAR',
			'pk'    => TRUE,
			'constraint' => 36, 
		),
		'name'	=> array(
			'type' => 'VARCHAR',
			'constraint' => 200, 
			'default'=>'',
			'validate'=>'trim|required',
		),

		'tel'	=> array(
			'type' => 'VARCHAR',
			'null'=>TRUE,
			'constraint' => 200, 
		),

		'email'	=> array(
			'type' => 'VARCHAR',
			'constraint' => 200, 
			'validate'=>'trim|required|valid_email',
		),
		'subject'		=> array(
			'type' => 'VARCHAR',
			'constraint' => 200, 
			'default'=>'',
			//'validate'=>'trim|required',
		),
		'content'		=> array(
			'type' => 'TEXT',
		),
		'ipaddr'		=> array(
			'type' => 'VARCHAR',
			'constraint' => 30, 
			'default'=>'',
		),
		'read_status'		=> array(
			'type' => 'INT',
			'constraint'=>'1',
			'default'=>'0',
		),
		'read_date'		=> array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'read_by'		=> array(
			'type' => 'VARCHAR',
			'constraint' => 200, 
			'null' => TRUE,
		),
		'read_by_id'		=> array(
			'type' => 'VARCHAR',
			'constraint' => 36, 
			'null' => TRUE,
		),
		'read_date'		=> array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
    );
}
